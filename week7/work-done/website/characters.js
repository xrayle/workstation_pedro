const characters = [
  { name: "Rick Sanchez", url: "profile-rick.html", photo: "rick.jpeg" },
  { name: "Morty Smith", url: "profile-morty.html", photo: "morty.jpeg" },
  { name: "Summer Smith", url: "", photo: "summer.jpeg" },
  { name: "Beth Smith", url: "", photo: "beth.jpeg" },
  { name: "Jerry Smith", url: "", photo: "jerry.jpeg" },
  { name: "Mr. Poopybutthole", url: "", photo: "Poopybutthole.jpeg" },
];

const charsTable = document.getElementById("side-bar");
for (i=0; i<characters.length;i++) {

    const route = `public/${characters[i].photo}`;
    const row = charsTable.insertRow(-1);
    row.innerHTML = `<td><a href=${characters[i].url}>${characters[i].name}</a></td>
    <td> <img src= ${route} alt=${characters[i].name} width="100" height="140"/></td>`;
}

const images = [
  {image: "funny-rick-morty.jpg",name:"Funny Rick and Morty"},
  {image: "rick-morty-boss.gif",name:"Rick Morty bosses"},
  {image: "skeeze-rick-morty.webp",name:"Skeezing Neck"},
  {image: "eyes-rick-morty.webp",name:"Showing Morty´s eyes"},
  {image: "finger.gif",name:"Middle Finger"},
  {image: "family-guns.jpg", name:"Family with guns"}
]

const imagesContainer = document.getElementById("images");

images.forEach(element => {
  const route = `public/${element.image}`;
  console.log(route);
  imagesContainer.innerHTML=`${imagesContainer.innerHTML} <td><img src= ${route} alt=${element.name} width="420" height="380"/></td>`;
})