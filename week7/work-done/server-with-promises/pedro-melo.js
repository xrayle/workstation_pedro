const fs = require("fs");

const data = "Hello world";
const filePath= "new-file.txt";

const promise = new Promise ((resolve,reject) => {

    fs.writeFile(filePath,data, (err) => {

    if (err) {
        reject(err);
    } else {
        resolve(data);
    }
})})

promise.then(data => console.log(`${data} written in file ${filePath}`)).catch((err => console.log(err)))

