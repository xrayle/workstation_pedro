function timer(value) {
    return new Promise((resolve) =>
    setTimeout(() => {
        resolve(value);
    }, Math.random() *100));
}

Promise.all([timer("first"), timer("second")]).then((data) => console.log(data));