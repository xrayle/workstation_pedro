
function sortBiggestToSmallest(numbers) {
    return numbers.sort(function(a,b) {
        return b-a;
    });
}
console.log(sortBiggestToSmallest([20, 10, 8, 80, -1, 2, -20, 3]));