function filterBannedWords(sentence, forbiddenWords) {
  var splitedSentence = sentence.split(" ");
    
  var allowedWords = splitedSentence.filter(function (element) {
    if (!forbiddenWords.includes(element)) {
      return true;
    }
    return false;
  });

  var finalSentence = allowedWords.reduce(function (acc, element) {
    return acc = acc + " " + element;
  });

  return finalSentence;
}
console.log(
  filterBannedWords("this is a dish with salmon, potatoes and salad", [
    "potatoes",
    "salad",
  ])
);
console.log(
    filterBannedWords("this is a dish with potatoes and salmon, potatoes and salad", [
      "potatoes",
      "salad",
    ])
  );
