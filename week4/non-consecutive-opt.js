function findConsecutiveNumbers(giveMeTheNumbers) {
    isConsecutive= false;
    var index;
    var nonConsecutiveNumbers = giveMeTheNumbers.filter(function(element,i) {
        
        while(isConsecutive === false && i<giveMeTheNumbers.length-1) {
            if (Math.abs(element +1 !== giveMeTheNumbers[i+1])) {
                index=i;
                return isConsecutive = true;

            } else {
                return isConsecutive = false;
            }
        }
    })
    nonConsecutiveNumbers=nonConsecutiveNumbers.concat(giveMeTheNumbers[index+1]);
    return nonConsecutiveNumbers;
  }
  
  console.log(findConsecutiveNumbers([1, 2, 20, 4, 5]));
  console.log(findConsecutiveNumbers([-1, 0, 4, 4, 5]));
  console.log(findConsecutiveNumbers([1, 2, 4, 4, 5]));
  console.log(findConsecutiveNumbers([3, 2, 4, -1, 2]));
  console.log(findConsecutiveNumbers([1, 2, 3, 1000, 5]));