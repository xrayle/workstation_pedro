/**
 * Return a prefixer function that concatenates a string argument with a prefix
 */
exports.stringPrefixer = function (prefix) {
    return function prefixer (string) {
        return prefix.concat(string);
    }
}

/**
 * Create an array of functions, each producing a result obtained
 * from applying the transform function to an argument from values array
 */
exports.makeResultFunctions = function (values, transform) {
        return values.map(function(number) {
           return fn = function () {
                return transform(number);
             }
    })
}

/**
 * From a function which receives three arguments,
 * of which only two are available, create a new function
 * which wraps the original one with the missing argument
 */
exports.createWrapperFunction = function (fn, arg1, arg2) {
    return function(arg3) {
        return fn(arg1,arg2,arg3);
    };
};
