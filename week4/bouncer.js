var queue = [
    { name: "John", age: 36},
    { name: "Alice", age: 24},
    { name: "Anna", age: 14},
    { name: "Rachel", age: 28},
    { name: "Alex", age: 50},
    { name: "Phil", age: 17}
];

var guestList = createGuestList(bounce(queue, 18));
console.log ("Tonight´s guest: ");
console.log(guestList);

function bounce (people,minAge) {
    return people.filter(function(person) {
        return person.age >= minAge;
    })
}

function createGuestList (people) {
    return people.map(function(guest) {
        return guest.name;
    })
}