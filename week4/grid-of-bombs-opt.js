function countingBombs(grid) {
    
  return (bombs = grid.reduce(function (acc, element) {
    if (element.includes(1)) {
      acc = acc + element.reduce(function (sum, bomb) {
          if (bomb === 1) {
            return (sum = sum + 1);
          }
          return sum;
        }, 0);
    }
    return acc;
  }, 0));
}
console.log(
  countingBombs([
    [0, 0, 0, 0],
    [0, 1, 0, 1],
    [0, 0, 1, 0],
    [0, 0, 0, 0],
  ])
);
