let key = document.getElementById("key-pressed");
let xyBlock = document.getElementById("coordinates")
let body = document.getElementById("body");
let main = document.getElementById("main");


body.addEventListener('keydown', getPressedKey);
main.addEventListener('pointermove', getCoordinates);

function getPressedKey (event) {
    key.textContent=event.key;
}

function getCoordinates (event) {
    console.dir(event);
    coordinates.textContent=`Pointer Coordinates: x: ${event.screenX.toFixed(5)} || y:${+event.screenY.toFixed(5)}`
}