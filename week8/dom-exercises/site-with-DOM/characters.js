const characters = [
  { name: "Rick Sanchez", url: "profile-rick.html", photo: "rick.jpeg" },
  { name: "Morty Smith", url: "profile-morty.html", photo: "morty.jpeg" },
  { name: "Summer Smith", url: "", photo: "summer.jpeg" },
  { name: "Beth Smith", url: "", photo: "beth.jpeg" },
  { name: "Jerry Smith", url: "", photo: "jerry.jpeg" },
  { name: "Mr. Poopybutthole", url: "", photo: "Poopybutthole.jpeg" },
];

const charsTable = document.getElementById("sidebar");
for (i=0; i<characters.length;i++) {

    const route = `public/${characters[i].photo}`;
    const row = charsTable.insertRow(-1);
    row.innerHTML = `<td><a href=${characters[i].url}>${characters[i].name}</a></td>
    <td> <img src= ${route} alt=${characters[i].name} width="100" height="140"/></td>`;
}
