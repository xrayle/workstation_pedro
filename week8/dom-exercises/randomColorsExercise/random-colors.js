let columns = document.querySelectorAll("#painting-div")
var buttons = document.querySelectorAll(".click");
var mainButton = document.getElementById("main-button")

mainButton.addEventListener("click", paintRandomColor);

buttons.forEach(button => button.addEventListener('click', paintSibbling))

function paintSibbling(event) {
  event.target.previousElementSibling.style.backgroundColor = randomColor();
  event.target.previousElementSibling.textContent = randomColor();
}

function paintRandomColor() {
  columns.forEach((column) => {
    column.textContent = randomColor();
    column.style.backgroundColor = randomColor();
  })
}

function randomColor() {
  let colorNumber =
    "#" + ((Math.random() * 0xffffff) << 0).toString(16).padStart(6, "0");
  return colorNumber;
}

