const cells = document.querySelectorAll(".grid");
const clearButton = document.querySelector("#clear");
const pallete = document.querySelectorAll(".painture");
const buttons = document.querySelectorAll(".button");
const main = document.getElementById("main");
let style = '#FFFFFF';
let isActive = false;

main.style.cursor="cell";

clearButton.addEventListener('click',clearColors);

cells.forEach(cell => cell.addEventListener('click', paintColor))

buttons.forEach(button => {
    button.addEventListener('mouseenter',highlightAndChangeCursor)
    button.addEventListener('mouseleave',removeHighlight)
})

pallete.forEach(color => color.addEventListener('click', saveTheColor))

function removeHighlight(event) {
    event.target.style.borderColor="white";
}

function highlightAndChangeCursor (event) {

    console.dir(event);

    event.target.style.borderColor="red";
    event.target.style.cursor ="pointer";
}

function clearColors() {

    cells.forEach(cell => cell.style.backgroundColor = '#FFFFFF');
}

function saveTheColor(event) {
    
    style = window.getComputedStyle(event.target);
    return style.backgroundColor;
}

function paintColor(event) {
    isActive=!isActive;

    if (isActive===true){
        cells.forEach(cell => cell.addEventListener('mouseenter',continuePainting))
    } else {
        cells.forEach(cell => cell.removeEventListener('mouseenter',continuePainting))
    }
    event.currentTarget.style.backgroundColor = style.backgroundColor;
}

function continuePainting(event) {

    event.currentTarget.style.backgroundColor = style.backgroundColor;
}

       




