function changeCharFrontBack(word) {
  var slicedWord = word.slice(1, word.length - 1);
  var wordIndex = word.length - 1;
  var changedWord = word[wordIndex] + slicedWord + word[0];

  return changedWord.toLowerCase();
}
console.log(changeCharFrontBack("heisenberg"));
console.log(changeCharFrontBack("DIOGO"));
