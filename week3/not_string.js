function addingNotToString(phrase) {
  var negationString = "not";
  var isPhraseNegated = phrase.includes(negationString);

  if (isPhraseNegated === false) {
    return "not" + phrase;

  } else {
    return phrase;
  }
}
console.log(addingNotToString("notsemicolon"));
console.log(addingNotToString("semicolon"));
console.log(addingNotToString("potato"));
console.log(addingNotToString("notpotato"));
