function manipulatingHeroes(dc, marvel) {
  var unsortedHeroesList = [];
  
  dc[0] = "Flash";
  console.log(dc);

  marvel[marvel.length - 1] = "Thor";
  console.log(marvel);

  unsortedHeroesList = dc.concat(marvel);
  console.log(unsortedHeroesList);
  avengersJLA = [];
  maxLength = unsortedHeroesList.length

  while (avengersJLA.length < maxLength) {
    var smallestSavedHero = unsortedHeroesList[0];

    for (var i = 0; i < unsortedHeroesList.length - 1; i++) {
      var nextHeroToCompare = unsortedHeroesList[i + 1];

      if (smallestSavedHero > nextHeroToCompare) {
        smallestSavedHero = nextHeroToCompare;
      }
    }
    avengersJLA.push(smallestSavedHero);

    unsortedHeroesList.splice(unsortedHeroesList.indexOf(smallestSavedHero), 1);
  }

  return avengersJLA.toString();
}
console.log(
  manipulatingHeroes(
    ["Constantine", "Batman", "Superman"],
    ["Captain America", "Hulk", "Wolverine"]
  )
);
