var numbers = [20, 24, 50, 60, 62, 91, 10001];

var sumOfNumbersEvens = numbers.reduce(function (sum, evens) {
  return evens % 2 === 0 ? sum + evens : sum;
}, 0);

console.log(sumOfNumbersEvens);
