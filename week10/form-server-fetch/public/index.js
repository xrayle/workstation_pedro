let submit = document.getElementById("submit");
const form = document.getElementById("form");
const table = document.getElementById("table");
const inputs = document.querySelectorAll(".input")
const clear = document.getElementById("clear");

submit.addEventListener("mouseenter",highlight);
submit.addEventListener("mouseleave",unHighlight);
clear.addEventListener("mouseenter",highlight);
clear.addEventListener("mouseleave",unHighlight);
form.addEventListener("submit", postMessages);
window.addEventListener('load', getMessages);
form.addEventListener("submit", getLastMessage);
clear.addEventListener('click', clearMessages);
window.addEventListener('keydown', postMessagesWithEnter)

function postMessagesWithEnter(event) {
    if (event.key==="Enter") {
        postMessages();
    } else {

    }
}

function clearMessages () {
    table.innerHTML='';
    letter = [];
    fetch("http://localhost:8080/clearMessages", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
            },
        body: JSON.stringify(letter)
    })
    .then(() => console.log('sucess'))
    .catch(err => console.err("Something bad happened" + err))
}

function postMessages (event) {
    
    //event.preventDefault();
    let date = new Date().toISOString();
    let user;
    
    if(inputs[0].value===''){
        user = 'Unknown';
    } else {
    user = inputs[0].value;
    }
    let textMessage = inputs[1].value;
    let letter = { timestamp: date, name: user, message: textMessage}
    
    fetch("http://localhost:8080/messages", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            location:'/'
            },
        body: JSON.stringify(letter)
    })
    .then(() => inputs.forEach(input => input.value=''))
    .then(() => window.location.replace("/"))
    .catch(err => console.err("Something bad happened" + err))
}
function getMessages () {
    fetch('http://localhost:8080/messages')
    .then(response => response.json())
    .then(data => {data.forEach(element => {
        let row = table.insertRow(-1);
        row.innerHTML = `<td> [<strong>Timestamp:</strong><span style="color:green">${element.timestamp}</span>]-<strong>Nickname:</strong> ${element.name} -> ${element.message} </td>`})})
    .catch(err => console.log('There was some error' + err))
}

function getLastMessage () {
    fetch('http://localhost:8080/messages')
    .then(response => response.json())
    .then(data => {
        let element = data[data.length-1];
        let row = table.insertRow(-1);
        row.innerHTML = `<td> [<strong>Timestamp:</strong><span style="color:green">${element.timestamp}</span>]-<strong>Nickname:</strong> ${element.name} -> ${element.message} </td>`})
    .catch(err => console.log('There was some error' + err))
}

function highlight (event) {
    event.target.style.border = "3px solid rgb(15, 155, 15)";
    event.target.style.cursor = "pointer";
}

function unHighlight (event) {
    event.target.style.border = "";
}


