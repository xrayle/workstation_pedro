const http = require("http");
const fs = require("fs");
const fsPromises = fs.promises;
const PORT = 8080;
let messages = [];

const mapper = {
  mp3: "audio/mp3",
  png: "image/png",
  mp4: "video/mp4",
  mpeg: "audio/mpeg",
  html: "text/html",
  css: "text/css",
  gif: "image/gif",
  jpg: "image/jpg",
  js: "text/javascript"
}

const routes = [
  { method: "GET", url: "/messages", func: getMessages },
  { method: "POST", url: "/messages", func: postHandler },
  { method: "POST", url: "/clearMessages", func: clearMessages },
  { method: "GET", url: /.*/, func: getHandler }
]

function getMessagesHandler (request, response) {
  
  let dataOfMessages = JSON.stringify(messages)
  response.writeHead(200, {"Content-Type":"application/json"});
  response.write(dataOfMessages);
  response.end();
}

function getMessages(request, response) {
  return getMessagesHandler(request, response)
}

function getHandler(request, response) {

  if (request.url === "/") {
    request.url = "/index.html";
  }

  const fileIndex = request.url.indexOf(".") + 1;
  const extension = request.url.substring(fileIndex);
  const promise = fsPromises.readFile(`${PUBLIC_ROOT}${request.url}`)

  promise.then(content => {
    console.log("[REQUEST] " + request.method + " " + request.url);
    response.writeHead(200, { "Content-Type": mapper[extension] });
    response.write(content)})

          .catch(() => response.writeHead(404))
          .finally (() => response.end());
}


function clearMessages(request, response) {
  request.on("data", function () {
    messages=[];
  })
  
  request.on("end", () => {
    response.writeHead(200);
    response.end()})
}

function postHandler(request, response) {

  request.on("data", function (chunk) {
  let payload = JSON.parse(chunk);
  if (payload.message==='/clear'){
    messages =[];
  } else {
  messages.push(payload)
  }})

  request.on("end", function (){
    response.writeHead(303, { Location: "/" });
    response.end();  
  })
}

const server = http.createServer(handler);
server.listen(PORT);
console.log("Server started @ localhost:" + PORT);
const PUBLIC_ROOT = "public";


function handler(request, response) {

  let { method, url } = request;

  console.log(method);

  routes.find(route => {
    if (method === route.method && url.match(route.url)) {
      route.func(request, response);
    }
  })
}


