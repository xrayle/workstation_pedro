const form = document.getElementById("form");
const input = document.getElementById("value");
const result = document.getElementById("result");
const select = document.getElementById("select");
let rates;

fetch("https://api.exchangeratesapi.io/latest")
    .then(response => response.json())
    .then(data => {
      rates = data.rates;
      const keys = Object.keys(data.rates);
      keys.forEach(key => select.innerHTML= select.innerHTML +`<option class="option" value=${key}>${key}</option>`)
      const option = document.querySelectorAll(".option")
    })
    
    .catch(err => console.error("Error fetching rates: ", err));


form.addEventListener("submit", sendData);

function sendData(event) {
  
  event.preventDefault();
  let selectedRate = select.value;
  let moneyConverted = input.value *rates[selectedRate];
      result.textContent=`Result of convert: ${moneyConverted}${selectedRate}`
      result.style.color='green';
}

/*apilist.fun*/
