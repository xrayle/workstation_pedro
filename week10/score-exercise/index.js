const submit = document.getElementById("submit");
const form = document.getElementById("form");
const div = document.getElementById("list");

form.addEventListener("submit", fetchData);

function fetchData(event) {
  
  div.innerHTML = "";
  event.preventDefault();
  const user = document.getElementById("input").value;
  fetch(`https://api.github.com/search/users?q=${user}&sort=joined&order=desc`)
    .then((response) => response.json())
    .then((data) => populateData(data))
    .catch(err => console.error("Invalid" + err));
}

function populateData (data) {
        let i = 0;
        let items = data.items;
        items.forEach((item) => {
          i++;
          let row = div.insertRow(-1);
          row.innerHTML = `<td class="data">${i}.-<strong>ID:${item.id}</strong>  USER ID:<a href="">${item.login}</a></td>`;
        })
}
