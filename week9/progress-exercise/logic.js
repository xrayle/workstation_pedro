let bar = document.getElementById("bar");
let button = document.getElementById("press");
let whiteColumns = document.querySelectorAll("#white-column");
let restart = document.getElementById("restart");
let index;

button.addEventListener('click', startLoading)
button.addEventListener('mouseover', highlight)
button.addEventListener('mouseleave',unhighlight)
restart.addEventListener('click', refreshColumns)
restart.addEventListener('mouseover', highlight)
restart.addEventListener('mouseleave',unhighlight)

button.style.cursor = "pointer";
restart.style.cursor = "pointer";

function refreshColumns () {
    whiteColumns.forEach(column => {
            column.style.backgroundColor="white";
            bar.textContent="";
})}

function startLoading () {
    index=0;
    whiteColumns.forEach(column => { 
        index=index+100;
        setTimeout(function () {
            column.style.backgroundColor="transparent"},index)})
    setTimeout(() =>bar.textContent="COMPLETE!",(whiteColumns.length)*100)
        }

function highlight(event) {
    event.target.style.borderColor = "red";
    console.dir(event);
}

function unhighlight(event) {
    event.target.style.borderColor = "black";
}
