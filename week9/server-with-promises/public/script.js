let submit = document.getElementById("submit");

submit.addEventListener("mouseenter",highlight);
submit.addEventListener("mouseleave",unHighlight);

function highlight (event) {
    event.target.style.border = "2px solid rgb(15, 155, 15)";
    event.target.style.cursor = "pointer";
}

function unHighlight (event) {
    event.target.style.border = "";
}


