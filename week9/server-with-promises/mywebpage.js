const http = require("http");
const fs = require("fs");
const fsPromises = fs.promises;
const PORT = 8080;
let messageArray = [];


const mapper = {
  mp3: "audio/mp3",
  png: "image/png",
  mp4: "video/mp4",
  mpeg: "audio/mpeg",
  html: "text/html",
  css: "text/css",
  gif: "image/gif",
  jpg: "image/jpg",
  js: "text/javascript",
}

const routes = [
  { method: "GET", url: "/", func: GETrequestHandler },
  { method: "GET", url: "/style.css", func: GETrequestHandler },
  { method: "POST", url: "/", func: POSTrequestHandler },
  { method: "GET", url: "/merry.gif", func: GETrequestHandler },
  { method: "GET", url: "/lindo.jpg", func: GETrequestHandler },
  { method: "GET", url: "/script.js", func: GETrequestHandler }
]

function GETrequestHandler(request, response) {
  if (request.url === "/") {
    request.url = "/index.html";
  }

  const fileIndex = request.url.indexOf(".") + 1;
  const extension = request.url.substring(fileIndex);

  fs.readFile(`${PUBLIC_ROOT}${request.url}`, function (err, content) {
    console.log(`${PUBLIC_ROOT}${request.url}`)
    if (err) {
      response.writeHead(404);
      response.write("Page Not Found");
    } else {
      console.log("[REQUEST] " + request.method + " " + request.url);
      response.writeHead(200, { "Content-Type": mapper[extension] });
      response.write(content);
    }
    response.end();
  })
}

function POSTrequestHandler(request, response) {

  let payload = "";
  request.on("data", function (chunk) {
    payload += chunk;
  })
  request.on("end", function () {
    let decodedPayload = decodeURIComponent(payload);
    let userMessageArray = decodedPayload.split('&');
    let name = userMessageArray[0].substring(9);
    let message = userMessageArray[1].substring(12);
    let date = new Date().toISOString();
    let regex = /\+/gi;

    let user = name.replace(regex, " ");
    let note = message.replace(regex, " ");

    messageArray.push({ timestamp: date, name: user, message: note })
    messageArray.forEach(element => console.log(`[timestamp:${element.timestamp}]-Nickname: ${element.name} -> ${element.message}`));
    
    response.writeHead(303, { Location: "/" });
    response.end();
    
  })
  
}

const server = http.createServer(handler);
server.listen(PORT);
console.log("Server started @ localhost:" + PORT);
const PUBLIC_ROOT = "public";


function handler(request, response) {

  let { method, url } = request;

  routes.forEach(route => {
    if (method === route.method && url === route.url) {
      route.func(request, response);
    }
  })
}


/*
const server = http.createServer(handler);
server.listen(PORT);
console.log("Server started @ localhost:" + PORT);
const PUBLIC_ROOT = "public";


function handler(request, response) {
  if (request.url === "/") {
    request.url = "/index.html";
  }
  const fileIndex = request.url.indexOf(".") + 1;
  const extension = request.url.substring(fileIndex);


  const promise = fsPromises.readFile(`${PUBLIC_ROOT}${request.url}`)

  promise.then(content => {
    console.log("[REQUEST] " + request.method + " " + request.url);
    response.writeHead(200, { "Content-Type": mapper[extension] });
    response.write(content)})

          .catch(err => {
    response.writeHead(404);
    response.write(err = "Page Not Found");})

          .finally (() => response.end());
}*/
/*----------------------asyncronous without promises-------------------------------------------------*/

/*fs.readFile(`${PUBLIC_ROOT}${request.url}`, function (err, content) {
  if (err) {
    response.writeHead(404);
    response.write("Page Not Found");
  } else {
    console.log("[REQUEST] " + request.method + " " + request.url);
    response.writeHead(200, { "Content-Type": mapper[extension] });
    response.write(content);
  }
  response.end();
});*/
/*-----------------------syncronous function------------------------------------------------------------

function handler (request,response) {
    if (request.url === "/") {
    var content = fs.readFileSync('index.html');
    console.log("[REQUEST] "+ request.method+ " "+request.url);
    response.writeHead(200);
    response.write(content);
    } else {
        response.writeHead(400);
        response.write('Page Not Found!');

    }
    response.end();
}
*/
