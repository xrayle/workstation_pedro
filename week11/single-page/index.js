const root = document.getElementById("root");
const title = document.createElement("h1");
title.setAttribute("id", "title");
let divImage;
let clickedProfile;
const bodyDiv = document.createElement("div");
bodyDiv.setAttribute("id", "main");

const router = [
  { url: /\/personages/, view: mainView },
  { url: /%+/, view: detailedView },
];

window.addEventListener("popstate", popPushStateHandler);

function popPushStateHandler() {
  let link = location.pathname;
  router.find((route) => {
    if (link.match(route.url)) {
      route.view(clickedProfile.id, clickedProfile.name);
    }
  });
}

mainView();

function mainView() {
  history.pushState("Main", "Main Page", "/personages");
  title.textContent = "Main Characters";
  cleanAndAppendDivs();

  fetch("https://rickandmortyapi.com/api/character")
    .then((response) => response.json())
    .then((data) => createMainView(data));
}

function detailedView(id, name) {
  history.pushState("Details", `${name} page`, `/${name}`);
  cleanAndAppendDivs();
  fetch(`https://rickandmortyapi.com/api/character/${id}`)
    .then((response) => response.json())
    .then((data) => createProfile(data));
}

function createImageDiv(filteredChars) {
  divImage = document.createElement("img");
  divImage.src = filteredChars.image;
  divImage.alt = filteredChars.name;
}

function createProfile(data) {
  const goBack = document.createElement("div");
  let characterName = data.name;
  title.textContent = characterName;
  createImageDiv(data);
  bodyDiv.appendChild(divImage);
  let profileDescription = document.createElement("div");
  profileDescription.setAttribute("id", "profile");
  console.log(data)
  
  profileDescription.innerHTML = `<p><strong>Created:</strong>${data.created}</p><br>
                              <p><strong>Gender:</strong>${data.gender}</p><br>
                              <p><strong>Location:</strong>${data.location.name}</p><br>
                              <p><strong>Origin:</strong>${data.origin.name}</p><br>
                              <p><strong>Specie:</strong>${data.species}</p><br>
                              <p><strong>Status:</strong>${data.status}</p><br>`;
  bodyDiv.appendChild(profileDescription);
  goBack.textContent = "Return to Main Page";
  root.appendChild(goBack);
  goBack.addEventListener("click", mainView);
}

function createMainView(data) {
  let characters = data.results;
  let filteredChars = characters.filter((element, index) => { return index < 4 ? element : false });

  filteredCharKeys = filteredChars.map(char => { return { name: char.name, image: char.image, id: char.id } });
  filteredCharKeys.forEach(char => createAllChars(char));
}

function createAllChars(char) {
  divName = document.createElement("div");
  divName.textContent = char.name;
  divName.id = char.name;

  createImageDiv(char);
  bodyDiv.appendChild(divName);
  bodyDiv.appendChild(divImage);
  divName.addEventListener("click", () => {
    clickedProfile = char;
    detailedView(char.id, char.name);
  })
}

function cleanAndAppendDivs() {
  root.innerHTML = "";
  bodyDiv.innerHTML = "";
  root.appendChild(title);
  root.appendChild(bodyDiv);
}