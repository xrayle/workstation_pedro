var hero = { name: 'Leeroy Jenkins' };
hero.damage = 10;
hero.hp = 130;
hero.armor = 7;

var batDamage = Math.round(Math.random() * (15 - 7) + 7);
var roundNumber = 0;
console.log('LEEEEEEEROOOY, JENKINS!');

while (hero.hp > 0) {

    roundNumber++;
    console.log('Round #' + roundNumber);

    hero.hp = hero.hp - (batDamage - hero.armor);
    console.log(hero.name + 'has ' + hero.hp + ' left.');
}

console.log('At least I have chicken...');