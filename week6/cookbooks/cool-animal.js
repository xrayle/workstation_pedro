function createCoolAnimals() {
  const species = ["goat", "cat", "dog", "cow", "chicken", "donkey"];
  const adjectives = ["beautiful", "ugly", "funny", "sexy", "muscular", "fat"];

  coolAnimals = [];
  species.forEach((animal) =>
    coolAnimals.push(
      adjectives[Math.floor(Math.random() * (adjectives.length - 1))] +
        " " +
        animal
    )
  );
  return coolAnimals;
}
const coolAnimalsOne = createCoolAnimals();
const coolAnimalsTwo = createCoolAnimals();
console.log(coolAnimalsOne);
console.log(coolAnimalsTwo);
