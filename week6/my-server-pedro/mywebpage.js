var http = require("http");
var fs = require("fs");
var PORT = 8000;

const mapper = {
  mp3: "audio/mp3",
  png: "image/png",
  mp4: "video/mp4",
  mpeg: "audio/mpeg",
  html: "text/html",
  plain: "text/plain",
  css: "text/css",
};

var server = http.createServer(handler);
server.listen(PORT);
console.log("Server started @ localhost:" + PORT);
const PUBLIC_ROOT = "public";

function handler(request, response) {
  if (request.url === "/") {
    request.url = "/index.html";
  }
  const fileIndex = request.url.indexOf(".") + 1;
  const extension = request.url.substring(fileIndex);

  fs.readFile(`${PUBLIC_ROOT}${request.url}`, function (err, content) {
    if (err) {
      response.writeHead(404);
      response.write("Page Not Found");
    } else {
      console.log("[REQUEST] " + request.method + " " + request.url);
      response.writeHead(200, { "Content-Type": mapper[extension] });
      response.write(content);
    }
    response.end();
  });
}

/*-----------------------syncronous function------------------------------------------------------------

function handler (request,response) {
    if (request.url === "/") {
    var content = fs.readFileSync('index.html');
    console.log("[REQUEST] "+ request.method+ " "+request.url);
    response.writeHead(200);
    response.write(content);
    } else {
        response.writeHead(400);
        response.write('Page Not Found!');

    }
    response.end();
}
*/
