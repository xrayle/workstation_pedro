/**
 * Should return array of number returning functions from 0 to times
 * Use block scope (not closure) to fix this
 */
exports.numberReturningFuncs = function(times) {

    const fs = [];

    for (let i = 0; i < times; i++) {
        fs.push(() => i);
    }

    return fs;
};

/**
 * Prevent variable reassignment
 */
exports.reassignVariable = function() {

    const a = 1;

    {
        const a = 1;
        a = 2;
    }
};
