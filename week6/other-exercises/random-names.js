const people = ["Pedro","Joana","Joao","Luis","Fabio","Bruno","Leandro","Gonçalo","Miguel","Sonia","Ines","André","Tiago",];

function sayingHello(people){
    const limit = Math.random() * 20;
    for (let k = 0; k < limit; k++) {
        setTimeout(() => {
            const index = Math.floor(Math.random() * people.length);
        console.log(`Hello, ${people[index]}`);
    }, k * 1000);
    }
} 

const helloPromise = new Promise ((resolve,reject) => {

    if (people===null) {
        reject('An error occured');
    }
    resolve (people);
})
helloPromise.then(sayingHello).catch(err => console.log(err))
