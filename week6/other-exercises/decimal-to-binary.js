function decimalToBinary(decimal) {
  let binary = [];

  while (decimal >= 2) {

    if (decimal % 2 !== 0) {
      decimal = Math.floor(decimal / 2);
      binary.push(1);
      continue;
    }
    decimal = decimal / 2;
    binary.push(0);
  }
  binary.push(Math.floor(decimal));
  binary = binary.reverse().join("");
  return Number.parseInt(binary)
}
const converter = decimalToBinary(150);
console.log(converter);
