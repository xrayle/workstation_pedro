function convertBinaryToDecimal(binary) {
  const number = binary.toString().split("").reverse();

  const numberInDecimal = number.reduce((acc, digit, index) => {
    if (digit === "1") {
      let powerOfTwo = getPowerOfTwo(index);

      return (acc = acc + powerOfTwo);
    }
    return acc;
  }, 0);

  function getPowerOfTwo(index) {
    let powerOfTwo = 1;
    if (index > 0) {
      for (let i = index; i > 0; i--) {
        powerOfTwo = powerOfTwo * 2;
      }
    }
    return powerOfTwo;
  }
  return numberInDecimal;
}
numberConverted = convertBinaryToDecimal(1111)
console.log(numberConverted);
