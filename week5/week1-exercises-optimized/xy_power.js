function findPowerResult(x, y) {
  let powerResult = x;

  for (let i = y; i > 1; i--) {
    powerResult = powerResult * x;
  }
  return powerResult;
}

let finalResult = findPowerResult(9, 3);
console.log(finalResult);
