function findSmallestNumber(numbers) {

  return numbers.reduce(
    (smallest, number) => number < smallest? smallest=number: smallest ,0)

}
const theSmallestNumber = findSmallestNumber([20, 10, 8, 80, -1, 2, -20, 3]);
console.log(theSmallestNumber);
