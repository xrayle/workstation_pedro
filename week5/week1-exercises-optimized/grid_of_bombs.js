function countingBombs(table) {

  return table.reduce((totalBombs, row) =>
      totalBombs += row.reduce((bombsInside, element) =>
          element === 1 ? (bombsInside += 1) : bombsInside,
        0
      ),
    0
  );
}
const totalBombsPlanted = countingBombs([
  [0, 0, 0, 0],
  [0, 1, 0, 1],
  [0, 0, 1, 0],
  [0, 0, 0, 0],
]);

console.log(totalBombsPlanted);
