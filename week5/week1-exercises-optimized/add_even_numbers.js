function calculatingsumOfEvens(arrayOfNumbers) {
  const totalSum = 
    arrayOfNumbers.reduce((acc, number) => (acc += number)
      , 0);

  return totalSum;
}

const sumOfEvensFunction = calculatingsumOfEvens([50, 25, 26, 23, 90, 54]);
console.log(sumOfEvensFunction);
