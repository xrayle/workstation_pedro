function calculateMean(myNumbers) {
  const mean =
    myNumbers.reduce((acc, number) => (acc += number)
      , 0) / myNumbers.length;

  return mean;
}
const calculateMeanFunction = calculateMean([25, 56, 45, 20]);
console.log(calculateMeanFunction);
