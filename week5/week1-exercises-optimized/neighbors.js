const numbersGiven = [-2, 0, 4, 26, 112, 113, 220];
let index;

function findNeighbors(numbersGiven) {
  let smallestDifference;
  const neighbors = [];
  for (let i = 0; i < numbersGiven.length - 1; i++) {
    let number = numbersGiven[i];
    let numberAfter = numbersGiven[i + 1];

    let difference = Math.abs(number - numberAfter);

    if (i === 0 || difference < smallestDifference) {
      smallestDifference = difference;
      index = i;
    }
  }
  neighbors.push(numbersGiven[index], numbersGiven[index + 1]);
  return neighbors;
}
const small = findNeighbors(numbersGiven);

console.log(small);
