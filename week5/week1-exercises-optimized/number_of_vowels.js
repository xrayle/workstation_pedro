function countingVowels(phrase) {
  const vowels = /[aeiou]/;
  const boxOfLetters = phrase.split("");

  return boxOfLetters.reduce((totalVowels, letter) =>
      vowels.test(letter) ? (totalVowels += 1) : totalVowels,
    0);
}
const countVowels = countingVowels("supercalifragilisticexpialidocious")
console.log(countVowels);
