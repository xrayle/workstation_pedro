function sortBiggestToSmallest(numbers) {
  const unsortedList = numbers;

  const sortedNumbers = unsortedList.sort((a,b) => b-a);

  return sortedNumbers;
}

const toSortNumbers = sortBiggestToSmallest([20, 10, 8, 80, -1, 2, -20, 3])
console.log(toSortNumbers);
