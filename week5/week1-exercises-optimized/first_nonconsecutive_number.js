function findNonConsecutiveNumbers(giveMeTheNumbers) {
  const nonConsecutives = [];

  const firstNumber = giveMeTheNumbers.find((number, index) =>
    number + 1 !== giveMeTheNumbers[index + 1] ? true : false
  );

  const indexOfSecondNumber = giveMeTheNumbers.indexOf(firstNumber) + 1;
  const secondNumber = giveMeTheNumbers[indexOfSecondNumber];
  
  nonConsecutives.push(firstNumber, secondNumber);

  return nonConsecutives;
}

const wantedNumbers = findNonConsecutiveNumbers([1, 2, 3, 4, 6]);
console.log(wantedNumbers);
