function countingVowels(phrase) {
  const vowels = /[aeiou]/;
  boxOfLetters = phrase.split("");

  const sumOfVowels = boxOfLetters.reduce(
    (acc, char) => (vowels.test(char) ? (acc += 1) : acc),
    0
  );

  return sumOfVowels;
}
const totalVowels = countingVowels("hello my name is pedro");
console.log(totalVowels);
