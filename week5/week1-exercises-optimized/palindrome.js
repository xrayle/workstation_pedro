function checkIfIsPalindrome(word) {
  let backwardIndex = word.length - 1;
  let forwardIndex = 0;

  while (forwardIndex < backwardIndex) {
    frontLetter = word[forwardIndex].toLowerCase();
    backLetter = word[backwardIndex].toLowerCase();

    if (frontLetter != backLetter ) {
      return `${word} is not a palindrome!`;
    }

    forwardIndex++;
    backwardIndex--;
  }

  return word + " is a palindrome";
}

palindromeTest = checkIfIsPalindrome("Racecar");
console.log(palindromeTest);

