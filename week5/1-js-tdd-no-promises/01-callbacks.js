/**
 * Invoke the callback and return the amount of time in miliseconds it took to execute
 */
exports.profileFunc = function(cb) {
    var startTime = Date.now();
    cb();
    var endTime =Date.now();
    return endTime - startTime;
};

/**
 * Invoke the async callback with the provided value after some delay
 */
exports.returnWithDelay = function(value, delay, cb) {

    const err = new Error("Error");
    setTimeout(function(err){
        cb(err,value)},delay);         
    }
/**
 * Invoke the async callback with an error after some delay
 */
exports.failWithDelay = function(delay, cb) {
    setTimeout(function (error) {
        error = new Error ("Error")
        cb(error)},delay);
}


