/**
 * Invoke the callback and return the amount of time in miliseconds it took to execute
 */
exports.profileFunc = function(cb) {
    var startTime = Date.now();
    cb();
    var endTime =Date.now();
    return endTime - startTime;
};

/**
 * Invoke the async callback with the provided value after some delay
 */
exports.returnWithDelay = function(value, delay, cb) {
    
    const err = new Error("Error");
    setTimeout(function(err){
        cb(err,value)},delay);   
};

/**
 * Invoke the async callback with an error after some delay
 */
exports.failWithDelay = function(delay, cb) {
    setTimeout(function (error) {
        error = new Error ("Error")
        cb(error)},delay);
};

/**
 * Return a promise that resolves after the specified delay
 * or rejects if the delay is negative or non-existent
 */
exports.promiseBasedDelay = function(delay) {
    
    return new Promise (function (resolved, rejected) {
        if (delay <=0) {
            rejected(delay)
        } else {
            setTimeout(resolved,delay)
        }   
    })
}
/**
 * Use fetch to grab the contents of both urls and return
 * a promise resolving to the payload concatenation
 */
exports.concatBodies = function(url1, url2) {
    var firstSite = fetch(url1);
    var secondSite = fetch(url2);
    var result = Promise.all([firstSite,secondSite])
    return result.then(result => result[0]+result[1]).catch();
};
