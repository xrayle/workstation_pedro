/**
 * Return a stop watch object with the following API:
 * getTime - return number of seconds elapsed
 * start - start counting time
 * stop - stop counting time
 * reset - sets seconds elapsed to zero
 */
exports.createStopWatch = function () {
    var timer = 0;
    var id;
    var stopWatch = {
        start: function () {
            
            id = setInterval(function () {
                timer++;
            }, 1000)
        },

        stop: function () {
            clearInterval(id);
        },

        reset: function () {
            timer = 0;
        },

        getTime: function () {
            return timer;
        }
    };
    return stopWatch;
};
