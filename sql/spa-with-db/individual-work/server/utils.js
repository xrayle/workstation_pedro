const formidable = require('formidable');

/**
 * Asynchronously gets and parses form data from a request payload
 *
 * @param {http.IncomingMessage} request client's request
 * @returns {Promise} promise that will resolve to the parsed data
 */
async function getFormFromPayload(request) {
    const form = formidable();

    return new Promise((resolve, reject) => {
        form.parse(request, function (err, data) {
            if (err) {
                reject(err);
                return;
            }

            resolve(data);
        });
    });
}

/**
 * Asynchronously gets and parses JSON from a request payload
 *
 * @param {http.IncomingMessage} request client's request
 * @returns {Promise} promise that will resolve to the JSON
 */
async function getJSONFromPayload(request) {
    let payload = '';

    return new Promise(function (resolve, reject) {
        request.on('data', function (data) {
            payload = payload + data;
        });

        request.on('end', function () {
            resolve(JSON.parse(payload));
        });

        request.on('error', reject);
    });
}

module.exports = {
    getFormFromPayload,
    getJSONFromPayload
};
