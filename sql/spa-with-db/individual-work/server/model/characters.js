const connectionPool = require ('./dbconnection');

/**
 * Adds a character
 *
 *
 * @returns {number} the new character id
 */
async function add(profile) {
    const query = `INSERT INTO profile(name, birthday, image, status, nickname, portrayed, category) VALUES (?,?,?,?,?,?,?)`;
    const {name, image, status, nickname, portrayed, category} = profile;
    const params = [name, 02/11/1992, image, status, nickname, portrayed, category];
    const [response] = await connectionPool.query(query,params);
    return response.insertId;
}

async function deleteChar (id){
    const query = `DELETE FROM profile WHERE id = ${id}`;
    const result = await connectionPool.query(query);
    
    return result[0].affectedRows;
}

async function put (profile, id) {
    const query = 'UPDATE profile SET name=?,birthday=?,image=?,status=?,nickname=?,portrayed=?,category=? WHERE id=?';
    const {name, image, status, nickname, portrayed, category} = profile;
    const params = [name,02/11/1992, image, status,nickname, portrayed, category, id];
    const [response] = await connectionPool.query(query, params);
    return response.affectedRows;
}

/**
 * Gets a character by its ID
 * Returns null if not found
 *
 * @param {number} id character's id
 * @returns {?object} the character
 */
async function get(id) {
    const query = `SELECT * FROM profile WHERE id=${id}`;
    const [rows] = await connectionPool.query(query);
    return rows[0];
}

/**
 * Lists all characters
 *
 * @param {object} [filter] optional filters
 * @returns {Array<object>} all characters
 */
async function list(filter = {}) {
    let query = `SELECT * FROM profile`;
    const { name } = filter;
    const params = [];
    
    if (name) {
        query += ` WHERE name LIKE ?`;
        params.push(`%${name}%`);
    }
    const [rows] = await connectionPool.query(query,params);
    return rows;
}

async function getComments (id) {
    query = `SELECT * FROM comments WHERE person_id=${id}`;
    const [rows] = await connectionPool.query(query);
    return rows;  
}



module.exports = {
    add,
    get,
    list,
    deleteChar,
    put,
    getComments
};
