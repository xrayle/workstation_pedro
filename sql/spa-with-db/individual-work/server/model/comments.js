const connectionPool = require ('./dbconnection');

async function addComment (data, id) {
    const query = `INSERT INTO comments (comment, user, person_id) VALUES (?,?,?)`;
    const {comment, user} = data;
    const person_id=id;
    const params = [comment, user, person_id];
    const [response] = await connectionPool.query(query,params);
    return response.insertId;
}

async function deleteComment (commentId,id) {
    const query = `DELETE FROM comments WHERE id = ${commentId} AND person_id = ${id} `;
    const result = await connectionPool.query(query);
    console.log(result[0].affectedRows)
    return result[0].affectedRows;
}

module.exports = {
    addComment,
    deleteComment
}