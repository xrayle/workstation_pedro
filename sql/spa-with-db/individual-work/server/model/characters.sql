/*CREATE DATABASE breaking_bad_chars;

USE breaking_bad_chars;*/

CREATE TABLE profile (
    id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    name VARCHAR(25) NOT NULL,
    birthday DATE NOT NULL,
    image VARCHAR(300) NOT NULL UNIQUE,
    status VARCHAR(30) NOT NULL,
    nickname VARCHAR(30) NOT NULL,
    portrayed VARCHAR(25) NOT NULL,  
    category VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE comments (
    id INTEGER AUTO_INCREMENT,
    comment VARCHAR(300) NOT NULL UNIQUE,
    user VARCHAR(50) NOT NULL,
    person_id INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (person_id) REFERENCES profile (id)
)

INSERT INTO profile VALUES 
(1,'Walter White',09/07/1958,'https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg','Presumed Dead', 'Heisenberg', 'Bryan Cranston', 'Breaking Bad'),
(2,'Jesse Pinkman',09/24/1984, 'https://vignette.wikia.nocookie.net/breakingbad/images/9/95/JesseS5.jpg/revision/latest?cb=20120620012441','Alive',"Cap n' Cook",'Aaron Paul','Breaking Bad'),
(3,'Skyler White',08/11/1970, 'https://s-i.huffpost.com/gen/1317262/images/o-ANNA-GUNN-facebook.jpg','Alive','Sky','Anna Gunn','Breaking Bad'),
(4,'Walter White',07/08/1993, 'https://media1.popsugar-assets.com/files/thumbor/WeLUSvbAMS_GL4iELYAUzu7Bpv0/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2018/01/12/910/n/1922283/fb758e62b5daf3c9_TCDBRBA_EC011/i/RJ-Mitte-Walter-White-Jr.jpg','Alive','Flynn','RJ Mitte','Breaking Bad'),
(5,'Henry Schrader',11/11/2000, 'https://vignette.wikia.nocookie.net/breakingbad/images/b/b7/HankS5.jpg/revision/latest/scale-to-width-down/700?cb=20120620014136','Deceased','Hank','Dean Norris','Breaking Bad'),
(6,'Marrie Schrader',11/11/2000, 'https://vignette.wikia.nocookie.net/breakingbad/images/1/10/Season_2_-_Marie.jpg/revision/latest?cb=20120617211645','Alive','Marie','Betsy Brandt','Breaking Bad'),
(7,'Mike Ehrmantraut',11/11/2000, 'https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_mike-ehrmantraut-lg.jpg','Deceased','Mike','Jonathan Banks','Breaking Bad'),
(8,'Saul Goodman',11/11/2000, 'https://vignette.wikia.nocookie.net/breakingbad/images/1/16/Saul_Goodman.jpg/revision/latest?cb=20120704065846','Alive','Jimmy McGill','Bob Odenkirk','Breaking Bad'),
(9,'Gustavo Fring',11/11/2000, 'https://vignette.wikia.nocookie.net/breakingbad/images/1/1f/BCS_S4_Gustavo_Fring.jpg/revision/latest?cb=20180824195925','Deceased','Gus','Giancarlo Esposito','Breaking Bad'),
(10,'Hector Salamanca',11/11/2000, 'https://vignette.wikia.nocookie.net/breakingbad/images/b/b4/Hector_BCS.jpg/revision/latest?cb=20170810091606','Deceased','Don Hector','Mark Margolis','Breaking Bad');