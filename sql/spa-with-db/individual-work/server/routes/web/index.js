const static = require('./static');

module.exports = [
    
    {
        url: /.*/,
        method: 'GET',
        handler: static
    }
];
