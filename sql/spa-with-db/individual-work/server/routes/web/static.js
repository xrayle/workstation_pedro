const fs = require('fs');
const path = require('path');

const PUBLIC_ROOT = 'website';

/**
 * Serves static assets under /public folder
 * Serves index.html if url does not include an extension
 *
 * @param {http.IncomingMessage} request client's request helper object
 * @param {http.ServerResponse} response server's response helper object
 */
function handler(request, response) {
    let url = request.url;
    const regex = /\/(details|filter|characters)\//;

    if (!path.extname(url)) {
        url = '/index.html';
    } else if (url.match(regex)) {
        url = `/${url.split(regex).pop()}`;
    }

    const file = path.join(PUBLIC_ROOT, url);
    console.log(file)

    if (fs.existsSync(file)) {
        const mimeType = getMIMEType(file);
        const content = fs.readFileSync(file);
        response.writeHead(200, { 'Content-Type': mimeType });
        response.write(content);
    } else {
        response.writeHead(404);
    }

    response.end();
}

module.exports = handler;

/**
 * guesses the MIME type for a file using its extension
 *
 * @param {string} file path to file
 * @returns {?string} best guess mime type or null
 */
function getMIMEType(file) {
    const MIME_TYPES = {
        html: 'text/html',
        js: 'text/javascript',
        css: 'text/css',
        map: 'application/json',
        json: 'application/json',
        jpeg: 'image/jpeg',
        jpg: 'image/jpeg',
        gif: 'image/gif',
        mp4: 'video/mp4',
        png: 'image/png',
        ico: 'image/x-icon',
        ttf: 'font/ttf'
    };

    const partials = file.split('.');
    const extension = partials[partials.length - 1];

    return MIME_TYPES[extension];
}
