const web = require('./web');
const api = require('./api');

const routes = api.concat(web);

/**
 * Gets the route object for an incoming request
 * The route object includes the request handler
 *
 * Returns null if no route matched the request
 *
 * @param {http.IncomingMessage} request client's request
 * @returns {?object} the route object
 */
function getRoute(request) {
    return routes.find((route) => {
        return matches(request, route);
    })
}

/**
 * Gets the url parameter values for a request
 *
 * @param {http.IncomingMessage} request client's request
 * @returns {object} object containing the url parameters
 */
function getParameters(request) {
    var route = getRoute(request);
    return route ? matches(request, route).groups : {};
}

module.exports = {
    getRoute,
    getParameters
};

/**
 * Checks whether a route object is the appropriate handler for a request
 *
 * @param {http.IncomingMessage} request client's request
 * @param {object} route route object
 * @returns {boolean} whether the route object should handle this request
 */
function matches(request, route) {
    if (route.method && route.method !== request.method) {
        return false;
    }
    return request.url.match(route.url);
}
