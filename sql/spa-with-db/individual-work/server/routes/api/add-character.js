const { profile } = require("console");
const { runInNewContext } = require("vm");
const jsonPayload = require("../../utils");
const {add} = require("../../model/characters")


async function specificHandler (request, response) {
    let payload = '';
    let id;
    request.on("data", async function (chunk) {
        payload += chunk;
    })
      
    request.on("end", async function (){
        let {name,image,status,nickname, portrayed, category} = JSON.parse(payload);
        const SQLData = {name,image,status,nickname, portrayed, category};
        console.log(SQLData);
        id = await add(SQLData);
        if (!id) {
            response.writeHead(404, {'Content-Type': 'application/json'});
            response.write(JSON.stringify({error:'Character not valid'}))
            response.end();  
            return;
        }
        response.writeHead(201, {'Content-Type': 'application/json'});
        response.write(JSON.stringify({id}));
        response.end();  
    })
}

module.exports = specificHandler;