const url = require('url');

const { list } = require('../../model/characters');


/**
 * Lists all characters as JSON
 *
 * @param {http.IncomingMessage} request client's request
 * @param {http.ServerResponse} response server's response helper object
 */
async function specificHandler(request, response) {
    const queryObject = url.parse(request.url, true).query;
    const data = await list(queryObject);
    const json = JSON.stringify(data);
    if (!json) {
        response.writeHead(404, { 'Content-Type': 'application/json' });
        response.write(JSON.stringify({err:"Characters Not Found"}));
        response.end();
        return;
    }
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.write(json);
    response.end();
}

module.exports = specificHandler;
