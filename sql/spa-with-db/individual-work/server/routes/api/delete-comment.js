const {deleteComment} = require ('../../model/comments');

async function specificHandler (request,response,params) {
    const id = Number.parseInt(params.id,10);
    let payload = '';
    request.on('data', function (chunk) {
        payload += chunk;
        
    })

    request.on('end', async function () {
        const identifier = JSON.parse(payload);
        const result = await deleteComment(identifier.commentId, id);

        if (!result) {
            response.writeHead(404, {'Content-Type':'application/json'});
            response.write(JSON.stringify({error:'Comment was not deleted'}));
            response.end();
            return;
        }
        response.writeHead(204, {'Content-type':'application/json'});
        response.write(JSON.stringify({result}));
        response.end();
    })
    
    
}

module.exports = specificHandler;