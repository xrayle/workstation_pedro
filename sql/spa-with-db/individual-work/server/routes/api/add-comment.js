const { request } = require("http");
const {addComment} = require("../../model/comments");

async function specificHandler (request,response,params) {
    const id = Number.parseInt(params.id,10);
    let payload ='';
    request.on('data', function (chunk) {
        payload += chunk;
    })

    request.on('end', async function () {
        const comment = JSON.parse(payload);
        const commentId = await addComment(comment,id);
        if (!commentId) {
            response.writeHead(404, {'Content-Type':'application/json'});
            response.end(JSON.stringify({error:'Comment was not added'}));
            return;
        }
        response.writeHead(201, {'Content-Type':'application/json'})
        response.write(JSON.stringify({commentId}));
        response.end();
    })
}

module.exports = specificHandler;