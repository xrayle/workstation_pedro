const {put} = require('../../model/characters');

async function specificHandler (request,response,params) {
    let payload = '';
    request.on('data', function (chunk) {
        payload += chunk;
    })
    request.on('end', async function () {
        const id = await Number.parseInt(params.id,10);
        console.log(payload);
        const data = JSON.parse(payload);
        const {name,image,status,nickname, portrayed,category} = data;
        const SQLData = {name,image,status,nickname,portrayed,category};
        const result = await put(SQLData, id);

        if (!result) {
            response.writeHead(404, {'Content-Type':'application/json'});
            response.write(JSON.stringify({error:'Character not valid'}));
            response.end();
            return;
        }

        response.writeHead(204, {'Content-Type': 'application/json'});
        response.write(JSON.stringify({id}));
        response.end();  
    })
}

module.exports = specificHandler;