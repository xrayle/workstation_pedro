const listCharacters = require('./list-characters');
const getCharacter = require('./get-character');
const addCharacter = require('./add-character');
const deleteCharacter = require('./delete-character');
const updateCharacter = require('./put-character');
const getComments = require('./get-comments');
const addComment = require('./add-comment');
const deleteComment = require('./delete-comment');

module.exports = [
    {
        url: /\/api\/character\/(?<id>\d+)\/?/,
        method: 'GET',
        handler: getCharacter
    },
    {
        url: /\/api\/character\/?/,
        method: 'GET',
        handler: listCharacters
    },
    {
        url: /\/api\/comment\/(?<id>\d+)\/?/,
        method:'GET',
        handler: getComments
    },
    {
        url: /\/api\/character\/?/,
        method: 'POST',
        handler: addCharacter
    },
    {
        url: /\/api\/comment\/(?<id>\d+)\/?/,
        method:'POST',
        handler: addComment
    },
    {
        url: /\/api\/character\/(?<id>\d+)\/?/,
        method: 'DELETE',
        handler: deleteCharacter
    },
    {
        url: /\/api\/comment\/(?<id>\d+)\/?/,
        method: 'DELETE',
        handler: deleteComment
    },
    {
        url: /\/api\/character\/(?<id>\d+)\/?/,
        method: 'PUT',
        handler: updateCharacter
    }
];
