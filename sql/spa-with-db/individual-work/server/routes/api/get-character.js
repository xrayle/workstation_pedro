const { get } = require('../../model/characters');

/**
 * Gets a character by its ID
 * ID is an url parameter
 *
 * Responds with 404 if character not found
 *
 * @param {http.IncomingMessage} request client's request
 * @param {http.ServerResponse} response server's response helper object
 * @param {object} params url parameter values
 */
async function specificHandler(request, response, params) {
    const id = Number.parseInt(params.id, 10);
    const character = await get(id);

    if (!character) {
        response.writeHead(404, {'Content-Type':'application/json'});
        response.write(JSON.stringify({error:'Character not valid'}));
        response.end();
        return;
    }
    const json = JSON.stringify(character);
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.write(json);
    response.end();
}

module.exports = specificHandler;
