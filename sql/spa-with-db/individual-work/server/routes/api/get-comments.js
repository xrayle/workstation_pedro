const {getComments} = require('../../model/characters');

async function specificHandler (request, response, params) {
    const id = Number.parseInt(params.id,10);
    const result = await getComments(id);
    if (!result) {
        response.writeHead(404, {'Content-Type':'application/json'});
        response.write(JSON.stringify({error:'Character not valid'}));
        response.end();
        return;
    }

    response.writeHead(200, {'Content-Type':'application/json'});
    response.write(JSON.stringify({result}));
    response.end();
}

module.exports = specificHandler;