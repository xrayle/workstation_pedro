const http = require('http');
const routes = require('./routes');

const PORT = 8000;

startServer(PORT);

/**
 * Starts a Web Server on the specified port
 *
 * @param {number} port port number
 */
function startServer(port) {
    const server = http.createServer(handler);
    server.listen(port);
    console.log('Server started @ localhost:' + port);
}

/**
 * Main request handler
 * Delegates the request to the appropriate handler
 *
 * Responds with 404 if route handler not found
 * Responds with 401 if route is protected and user is not authorized
 *
 * @param {http.IncomingMessage} request client's request
 * @param {http.ServerResponse} response server's response helper object
 */
async function handler(request, response) {
    console.log('[REQUEST] ' + request.method + ' ' + request.url);

    const route = routes.getRoute(request);
    const parameters = routes.getParameters(request);

    if (!route) {
        response.writeHead(404);
        response.end();
        return;
    }
    route.handler(request, response, parameters);
}
