import { getDataFromAPI, filterActorsByName } from "../service/service.js";
import { startLoadingBar} from "../view/utilities/components.js";
import { populatingData } from "../view/personages/personages.js";
import renderErrorMessage from "../view/error.js";

async function getDataToPopulate(pageNumber) {

  startLoadingBar();
  try {
    const personages = await getDataFromAPI(pageNumber);
    prepareData(personages);
  }
  catch (error) {
    console.log(error)
    renderErrorMessage();
  }
}

async function getFilteredPage(name) {

  startLoadingBar();
  try {
  const personages = await filterActorsByName(name);

  prepareData(personages);
  }
  catch (error) {
    
    renderErrorMessage();
  }
}

function prepareData (personages) {
  
  const data = personages.map((personage) => {
      return {id:personage.id, name: personage.name, img: personage.image, status: personage.status };
  });
  populatingData(data);
}

export {getDataToPopulate, getFilteredPage};

