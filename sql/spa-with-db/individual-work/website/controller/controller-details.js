import  {getActorFromAPIById,getComments,deleteCommentFromDB,addComment} from "../service/service.js";
import {getProfileDetails, messagesSection, getAllCharComments} from "../view/personage-detail/details.js";
import {startLoadingBar} from "../view/utilities/components.js";
import renderErrorMessage from "../view/error.js";

async function deleteComment(event) {
  const commentId = event.target.id;
  const id = window.location.pathname.split('/').slice(-1)[0];
  console.log(commentId);
  await deleteCommentFromDB(commentId,id);
  getAllCharComments(serviceFunctions,id);
}

async function addComments(event){
  messagesSection.innerHTML='';
  event.preventDefault();
  const id = window.location.pathname.split('/').slice(-1)[0];
  const comment = event.target.form[0].value;
  const user = event.target.form[1].value;
  const profileMessage = {comment,user};
  await addComment(profileMessage,id);
  getAllCharComments(serviceFunctions,id);
}

async function getAllComments(id) {
  const data = (await getComments(id)).result;
  return data;
}

const serviceFunctions = {getAllComments, addComments, deleteComment};

async function populateData (identifier) {
  
  startLoadingBar();
  try {
    let data = await getActorFromAPIById(identifier);
    const messages = (await getComments(identifier)).result;
    let { id, name, image, status, nickname, portrayed, category} = data;
    let filteredData = { id, name, image, status, nickname, portrayed, category};
    getProfileDetails(filteredData, serviceFunctions, id);
  }
  catch (error) {
    console.log(error)
    renderErrorMessage();
  }
}

export default populateData;
