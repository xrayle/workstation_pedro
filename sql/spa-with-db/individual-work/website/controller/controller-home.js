import { renderHomePage, countEpisodesBySeason } from "../view/home/home.js";
import renderErrorMessage from "../view/error.js";
import { getRandomQuote, getAllEpisodes } from "../service/service.js";
import { startLoadingBar } from "../view/utilities/components.js";

async function getDataForHomePage() {
  startLoadingBar();
  try {
    const data = await getRandomQuote();
    const episodesData = await getAllEpisodes();
    const { author, quote } = data[0];
    const filteredData = { author, quote };
    const filteredEpisodes = episodesData.map((movie) => {
      return { episode: movie.episode, season: movie.season };
    });
    renderHomePage(filteredData);
    countEpisodesBySeason(filteredEpisodes);
  } catch (error) {
    renderErrorMessage(error);
  }
}

export default getDataForHomePage;
