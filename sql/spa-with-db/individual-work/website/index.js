import navigate from "./navigation.js";

let path = location.pathname;
navigate(path);

window.addEventListener("popstate", () => {
  let flag = true;
  navigate(location.pathname, flag);
});
