import { ROUTES, getRoute } from "./router.js";

function navigate(path, flag) {
  if (!flag) {
    history.pushState({ path }, null, path);
  }

  const route = getRoute(path);

  if (!route) {
    navigate(ROUTES[0].route);
    return;
  }

  const identifier = getURLParam(path);
  route.controller(identifier);
}

function getURLParam(path) {
  const parameter = path.split("/").slice(-1);
  return parameter;
}

export default navigate;
