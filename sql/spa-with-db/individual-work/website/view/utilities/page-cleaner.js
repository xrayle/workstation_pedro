import { root, header, filter, navigator, mainBox} from "./components.js";
import { appearancesBox, detailsBox } from "../personage-detail/details.js";

function cleanHomePage() {
  if (header.lastChild === filter) {
    header.removeChild(filter);
  }
  removeNavigator();
  removeAppearances();
}

function removeNavigator() {
  root.style.backgroundImage = "none";
  mainBox.innerHTML = "";
  detailsBox.innerHTML = "";
  if (root.contains(navigator)) {
    root.removeChild(navigator);
  }
}

function removeAppearances() {
  root.style.backgroundImage = "none";
  mainBox.innerHTML = "";
  navigator.innerHTML = "";
  if (root.contains(appearancesBox)) {
    root.removeChild(appearancesBox);
  }
}

export { cleanHomePage, removeAppearances, removeNavigator };
