import navigate from "../../navigation.js";

const root = document.getElementById("root");
const mainHeader = document.createElement("header");
const home = document.createElement("div");
const header = document.createElement("section");
const title = document.createElement("div");
const characters = document.createElement("figure");
const mainBox = document.createElement("div");
const filter = document.createElement("input");
const navigator = document.createElement("div");

mainHeader.setAttribute("id", "main-header");
home.setAttribute("id", "home");
header.setAttribute("id", "header");
filter.setAttribute("placeholder", "Search here for the character");
title.setAttribute("id", "title");
characters.setAttribute("id", "characters");
filter.setAttribute("id", "filter");
mainBox.setAttribute("id", "main");
navigator.setAttribute("id", "navigator");

root.appendChild(mainHeader);
mainHeader.appendChild(home);
mainHeader.appendChild(header);
mainHeader.appendChild(characters);
header.appendChild(title);
root.appendChild(mainBox);
root.appendChild(navigator);

home.addEventListener("click", goHome);
home.addEventListener("mouseenter", zoomIn);
home.addEventListener("mouseleave", zoomOut);
characters.addEventListener("click", goCharacters);
characters.addEventListener("mouseenter", zoomIn);
characters.addEventListener("mouseleave", zoomOut);

characters.innerHTML = `<p id="chars"> Character's Main Page </p>`;

function zoomIn(event) {
  event.target.style.cursor = "pointer";
  event.target.style.border = "1px inset black";
  event.target.style.color = "red";
}

function zoomOut(event) {
  event.target.style.border = "none";
  event.target.style.color = "white";
}

function goHome() {
  navigate(`/`);
}

function goCharacters() {
  navigate(`/characters/`);
}

function startLoadingBar() {
  mainBox.innerHTML = "";
  let loading = document.createElement("div");
  loading.innerHTML = `<img src="../public/loading.gif" alt="loading bar" width="300" height="300">`;
  mainBox.appendChild(loading);
}

function addFilter() {
  if (header.lastChild !== filter) {
    header.appendChild(filter);
  }
}

export { root, home, mainHeader, header, navigator, title, filter, mainBox};
export { startLoadingBar, addFilter };
