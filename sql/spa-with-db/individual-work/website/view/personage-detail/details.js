import { root, title, mainBox} from "../utilities/components.js";
import { removeNavigator } from "../utilities/page-cleaner.js";

const profileBox = document.createElement("div");
const imageBox = document.createElement("img");
const detailsBox = document.createElement("section");
const appearancesBox = document.createElement("div");
const messagesSection = document.createElement('section');
profileBox.setAttribute("id", "profile-box");
imageBox.setAttribute("id", "actor-image");
appearancesBox.setAttribute("id", "appearances-container");
root.appendChild(messagesSection);

function getProfileDetails(data, serviceFunctions, id) {
  removeNavigator();
  profileBox.innerHTML = "";

  title.textContent = data.name;
  imageBox.src = data.image;
  imageBox.alt = data.name;
  mainBox.appendChild(profileBox);
  profileBox.appendChild(imageBox);

  printProfileDetails(data);
  getAllCharComments(serviceFunctions, id)
  sendComments(serviceFunctions)
}

function printProfileDetails(data) {
  const seasons = document.createElement("div");
  const job = document.createElement("ul");
  const {status, nickname, portrayed, category} = data;
  const profileDetails = [
      `Status: ${status}`,
      `Nickname: ${nickname}`,
      `Portrayed: ${portrayed}`,
      `Category: ${category}`
  ];

  seasons.setAttribute("id", "appearances");
  job.setAttribute("id", "occupation");

  profileDetails.forEach((detail) => {
    detailsBox.innerHTML =
      detailsBox.innerHTML + `<div id="detail"> ${detail} </div>`;
  });
  profileBox.appendChild(detailsBox);
}
  async function getAllCharComments (serviceFunctions, id) {

   const comments = await serviceFunctions.getAllComments(id);
    messagesSection.innerHTML ='';

    comments.forEach(comment => {
      const message = document.createElement('p');
      const remove = document.createElement('button');
      remove.setAttribute('type','button')
      remove.innerText='Delete';
      remove.setAttribute('id',`${comment.id}`);
      message.setAttribute('id',`${comment.id}`);
      message.innerText = `${comment.comment} --------->user: ${comment.user}`;
      messagesSection.appendChild(message);
      messagesSection.appendChild(remove);
      remove.addEventListener('click', serviceFunctions.deleteComment);
    }
    )
  }

  async function sendComments(serviceFunctions) {
    const form = document.createElement('form');
    const userLabel = document.createElement('label');
    const userInput = document.createElement('input');
    const messageLabel = document.createElement('label');
    const messageInput = document.createElement('input');
    const submit = document.createElement('input');
    form.setAttribute('id','form');
    submit.setAttribute('type','submit');
    submit.setAttribute('value','Submit')
    submit.setAttribute('id','submit');
    setInput(userInput,'text','userInput');
    setInput(messageInput,'text','messageInput');
    userLabel.textContent = 'User:';
    messageLabel.textContent='Comment:';
    submit.textContent='Submit';
    mainBox.appendChild(form);
    form.appendChild(userLabel);
    form.appendChild(userInput);
    form.appendChild(messageLabel);
    form.appendChild(messageInput);
    form.appendChild(submit);
    submit.addEventListener('click', serviceFunctions.addComments);
  }

  function setInput (input, type, name){
    input.setAttribute('type',`${type}`);
    input.setAttribute('name',`${name}`)
  }

  function commentSent (){}
  



















  /*job.textContent = "Jobs: ";
  occupation.forEach((work) => {
    job.innerHTML = `${job.innerHTML}<li id='job'>${work}</li>`;
  });*/

  //appearancesBox.innerHTML = `<p>Appearances:</p>`;

  //profileBox.appendChild(job);
  /*root.appendChild(appearancesBox);
  appearancesBox.appendChild(seasons);

  appearance.forEach((show) => {
    seasons.innerHTML = `${seasons.innerHTML}<br><p class="appearance">Season ${show}</p>`;
  });
}*/
export { getProfileDetails, appearancesBox, detailsBox, messagesSection, getAllCharComments};
