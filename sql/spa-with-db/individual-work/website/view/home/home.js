import { title, mainBox} from "../utilities/components.js";
import { cleanHomePage } from "../utilities/page-cleaner.js";

const space = `\u00A0\u00A0\u00A0\u00A0`;

function renderHomePage(data) {
  const quote = document.createElement("div");
  quote.setAttribute("id", "quote");

  cleanHomePage();

  quote.innerHTML = `<p> <span style="font-style:italic;">"${data.quote}" ${space} </span>${data.author}</p>`;
  mainBox.appendChild(quote);

  setSerieSinopse();
}

function setSerieSinopse() {
  const sinopse = document.createElement("div");
  const video = document.createElement("div");
  sinopse.setAttribute("id", "sinopse-box");
  mainBox.setAttribute("class", "main-home");
  video.innerHTML = `<video controls src="../public/breaking-bad-trailer.mp4" poster="https://images4.alphacoders.com/258/thumb-1920-258026.png"
    width="850"> Sorry, your browser doesn't support embedded videos </video>`;

  sinopse.innerHTML = `<p id="sinopse"> ${space} Set in Albuquerque, New Mexico, between 2008 and 2010,[12] Breaking Bad follows Walter White, a meek high school chemistry teacher who transforms into a ruthless player in the local methamphetamine drug trade, driven by a desire to provide for his family after being diagnosed with terminal lung cancer. Initially making only small batches of meth with his former student Jesse Pinkman in a rolling meth lab, Walter and Jesse eventually expand to make larger batches of a special blue meth that is incredibly pure and creates high demand. Walter takes on the name "Heisenberg" to mask his identity. Because of his drug-related activities, Walter eventually finds himself at odds with his family, the Drug Enforcement Administration (DEA) through his brother-in-law Hank Schrader, the local gangs, the Mexican drug cartels and their regional distributors, putting his life at risk.</p>`;
  sinopse.innerHTML =
    sinopse.innerHTML +
    `<p id="genres">Genres: <span style= "color:red;">Crime </span> |<span style= "color:grey;"> Drama </span>| <span style= "color:brown;">Thriller</span></p>`;
  mainBox.appendChild(sinopse);
  mainBox.appendChild(video);
  title.textContent = "Breaking Bad Synopsis";
}

function countEpisodesBySeason(data) {
  const episodesMain = document.createElement("section");
  const index = 0;
  const seasonNumber = [1, 2, 3, 4, 5];
  const episodesCounting = {
    first: index,
    second: index,
    third: index,
    fourth: index,
    fifth: index,
  };
  const keys = Object.keys(episodesCounting);
  const season = document.createElement("div");

  episodesMain.setAttribute("id", "episodes-main");
  season.setAttribute("id", "seasons-episodes");
  episodesMain.innerHTML = `<p id='episodes-main-title'>TOTAL EPISODES OF EACH SEASON!</p>`;
  mainBox.appendChild(episodesMain);

  data.forEach((movie) => {
    if (movie.season == seasonNumber[0]) {
      episodesCounting.first++;
    } else if (movie.season == seasonNumber[1]) {
      episodesCounting.second++;
    } else if (movie.season == seasonNumber[2]) {
      episodesCounting.third++;
    } else if (movie.season == seasonNumber[3]) {
      episodesCounting.fourth++;
    } else if (movie.season == seasonNumber[4]) {
      episodesCounting.fifth++;
    }
  });

  keys.forEach((key, index) => {
    season.innerHTML = `${season.innerHTML}<div id="season">Season ${index + 1}<p id="total-episodes">${episodesCounting[key]}</p></div>`;
    episodesMain.appendChild(season);
  });
}

export { renderHomePage, countEpisodesBySeason };
