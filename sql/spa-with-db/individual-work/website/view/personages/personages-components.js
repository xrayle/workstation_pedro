import navigate from "../../navigation.js";
import { mainBox, root } from "../utilities/components.js";
import { cleanHomePage, removeAppearances, removeNavigator } from "../utilities/page-cleaner.js";

function imageZoomIn(event) {
  event.target.style.transform = "scale(1.5)";
  event.target.style.border = "1px solid red";
  event.target.style.cursor = "pointer";
}

function imageZoomOut(event) {
  event.target.style.transform = "scale(1)";
  event.target.style.border = "1px solid rgba(0, 0, 0, 0.5)";
}

function seeCharDetails(event) {
  navigate(`/details/${event.target.id}`);
}

function filterData(event) {
  if (event.key === "Enter") {
    navigate(`/filter/${event.target.value}`);
    event.target.value = "";
  }
}

function highlight(event) {
  event.target.style.border = "2px solid red";
  event.target.style.cursor = "pointer";
}

function unHighlight(event) {
  event.target.style.border = "none";
}

function goPageBefore(event) {
  let path = document.location.pathname;

  if (path === "/characters/1") {
    navigate(`/characters/`);
  } else if (parseInt(`${path}`.split("/").slice(-1)) > 0) {
    let pageNumber = parseInt(`${path}`.split("/").slice(-1));
    navigate(`/characters/${pageNumber - 1}`);
  }
}

function goNextPage() {
  let path = document.location.pathname;

  if (path === "/characters/") {
    let i = 1;
    navigate(`/characters/${i}`);
  } else {
    let pageNumber = parseInt(`${path}`.split("/").slice(-1));

    if (pageNumber < 5) {
      navigate(`/characters/${pageNumber + 1}`);
    }
  }
}

function addCharacter () {
  cleanHomePage();
  mainBox.innerHTML='';
  const form = document.createElement('form');
  const nameLabel = document.createElement('label');
  const nameInput = document.createElement('input');
  const statusLabel = document.createElement('label');
  const statusInput = document.createElement('input');
  const imageLabel = document.createElement('label');
  const imageInput = document.createElement('input');
  const nickLabel = document.createElement('label');
  const nickInput = document.createElement('input');
  const portrayedLabel = document.createElement('label');
  const portrayedInput = document.createElement('input');
  const categoryLabel = document.createElement('label');
  const categoryInput = document.createElement('input');
  const submit = document.createElement('input')
  form.setAttribute('id','form');
  form.setAttribute('action','http://localhost:8000/submit')
  form.setAttribute('method','POST');
  submit.setAttribute('type','submit');
  submit.setAttribute('value','Submit')
  submit.setAttribute('id','submit');
  setInput(nameInput,'text','nameInput');
  setInput(statusInput,'text','statusInput');
  setInput(imageInput,'text','imageInput');
  nameLabel.textContent = 'Name:';
  statusLabel.textContent='Status:';
  imageLabel.textContent='Image URL:';
  nickLabel.textContent = 'Nickname:';
  portrayedLabel.textContent = 'Portrayed:';
  categoryLabel.textContent = 'Category:';
  submit.textContent='Submit';
  mainBox.appendChild(form);
  form.appendChild(nameLabel);
  form.appendChild(nameInput);
  form.appendChild(imageLabel);
  form.appendChild(imageInput);
  form.appendChild(statusLabel);
  form.appendChild(statusInput);
  form.appendChild(nickLabel);
  form.appendChild(nickInput);
  form.appendChild(portrayedLabel);
  form.appendChild(portrayedInput);
  form.appendChild(categoryLabel);
  form.appendChild(categoryInput);
  form.appendChild(submit);
  submit.addEventListener('click',sendData)
}

function setInput (input, type, name){
  input.setAttribute('type',`${type}`);
  input.setAttribute('name',`${name}`)
}

function sendData(event) {
  event.preventDefault();
  let charName = event.target.form[0].value;
  let charImage = event.target.form[1].value;
  let charStatus = event.target.form[2].value;
  let charNick = event.target.form[3].value;
  let charPortrayed = event.target.form[4].value;
  let charCategory = event.target.form[5].value;

  const info = {name:charName,image:charImage,status:charStatus,nickname:charNick,portrayed:charPortrayed,category:charCategory};
  fetch("http://localhost:8000/submit", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            location:'/api/characters/'
            },
        body: JSON.stringify(info)
  })
  .then(response => console.log(response))
  //.then(() => window.location.replace('/characters/'))
  .catch(() => console.log('something bad happened'))
}

export {imageZoomIn,imageZoomOut,seeCharDetails,filterData,highlight,unHighlight,goPageBefore,goNextPage, addCharacter};
