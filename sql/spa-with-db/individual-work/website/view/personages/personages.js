import { root, title, filter, mainBox,navigator, addFilter, header } from "../utilities/components.js";
import { removeAppearances} from "../utilities/page-cleaner.js";
import { imageZoomIn, imageZoomOut, seeCharDetails, filterData, highlight, unHighlight, goPageBefore, goNextPage, addCharacter} from "./personages-components.js";
const newCharDiv = document.createElement("div");

function populatingData(data) {
  newCharDiv.setAttribute('id','addCharacter');
  newCharDiv.textContent='+ ADD CHARACTER'
  removeAppearances();
  header.appendChild(filter);
  
  data.forEach((char) => {

    populateElements(char);
  });
  mainBox.appendChild(newCharDiv);
  addPagination();
}

function populateElements(char) {
  const actorBox= document.createElement("div");
  const image = document.createElement("img");
  const name = document.createElement("div");
  const status = document.createElement("div");

  actorBox.setAttribute("class", "box");
  actorBox.setAttribute("id",`${char.id}`)
  image.setAttribute("id", `${char.id}`);
  name.setAttribute("id", "name");
  status.setAttribute('id','status-container');

  title.textContent = "C h a r a c t e r s";
  image.src = char.img;
  image.alt = char.name;
  name.textContent = char.name;

  setStatus(char.status, status);
  
  actorBox.appendChild(image);
  actorBox.appendChild(name);
  actorBox.appendChild(status);
  mainBox.appendChild(actorBox);

  image.addEventListener('mouseenter', imageZoomIn);
  image.addEventListener('mouseleave', imageZoomOut);
  actorBox.addEventListener('click', seeCharDetails);
}

function setStatus (status, statusBox) {
  let imageStatus = document.createElement('img');
  let state = document.createElement('p')

  imageStatus.setAttribute('class','image-status');
  state.setAttribute('id','status');

  state.textContent = 'Status: ';
  statusBox.appendChild(state);

  switch (status) {

    case 'Deceased':
    imageStatus.src='../public/skulll.gif';
    imageStatus.alt='Dead';
    imageStatus.title = 'Dead';
    break;

    case 'Alive':
    imageStatus.src='../public/alivee.gif';
    imageStatus.alt='Alive';
    imageStatus.title = 'Alive';
    break;

    default:
    imageStatus.src='../public/isalive.gif';
    imageStatus.alt='Presumed Dead';
    imageStatus.title = 'Presumed Dead';
    break;
  }
  statusBox.appendChild(imageStatus);
}

function addPagination () {
  const navigateForward = document.createElement('div');
  const navigateBackward = document.createElement('div');

  navigateForward.setAttribute('id','go-forward');
  navigateBackward.setAttribute('id','go-back');

  const path = document.location.pathname;
  const pageNumberBackward = path.split('characters').slice(-1)[0];
  const pageNumberForward = parseInt(`${path}`.split('/').slice(-1));

  if (pageNumberBackward!=='/'&&!path.match(/\/filter/)) {
    navigator.appendChild(navigateBackward);
  }
    
  if (pageNumberForward!==5&&!path.match(/\/filter/)) {
    navigator.appendChild(navigateForward);
  }
  
  navigateBackward.addEventListener('click', goPageBefore);
  navigateBackward.addEventListener('mouseenter', highlight);
  navigateBackward.addEventListener('mouseleave', unHighlight);
  navigateForward.addEventListener('mouseenter', highlight);
  navigateForward.addEventListener('mouseleave', unHighlight);
  navigateForward.addEventListener('click', goNextPage);

  if(root.lastChild!==navigator) {
    root.appendChild(navigator);
  }
}

filter.addEventListener('keyup', filterData);
newCharDiv.addEventListener('click', addCharacter)

export { populatingData };
