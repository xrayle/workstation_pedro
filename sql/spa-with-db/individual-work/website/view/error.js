import { root, title } from "./utilities/components.js";
import { cleanHomePage } from "./utilities/page-cleaner.js";

function renderErrorMessage() {
  cleanHomePage();
  root.style.height = "100vh";
  root.style.margin = "0";
  root.style.backgroundImage = 'url("../public/danger.jpg")';
  root.style.backgroundSize = "95%";
  title.innerHTML = '<p style="color:red;">Something Bad Happened </p>';
}

export default renderErrorMessage;
