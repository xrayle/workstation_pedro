import { getDataToPopulate, getFilteredPage} from "./controller/controller-personages.js";
import populateData from "./controller/controller-details.js";
import getDataForHomePage from "./controller/controller-home.js";

const ROUTES = [
    { halfRoute: "/home" }, 
    { halfRoute: "/filter" }
];

const routes = [
  { path: /\/filter/, controller: getFilteredPage },
  { path: /\/details\/\d+/, controller: populateData },
  { path: /\/characters\//, controller: getDataToPopulate },
  { path: /\//, controller: getDataForHomePage },
];

function getRoute(path) {
  return routes.find((route) => {
    return path.match(route.path);
  });
}

export { ROUTES, getRoute };
