const endpoint = "https://www.breakingbadapi.com/api";
const myEndpoint = "http://localhost:8000/api";

async function getDataFromAPI(pageNumber) {
  const response = await fetch(`${myEndpoint}/character`);
  const data = await getDataFromResponse(response);
  return fixAllCharData(data);
}

async function getActorFromAPIById(identifier) {
  const response = await fetch(`${myEndpoint}/character/${identifier}`);
  const data = await getDataFromResponse(response);
  return data;
}

async function filterActorsByName(name) {
  const response = await fetch(`${myEndpoint}/character?name=${name}`);
  const data = await getDataFromResponse(response);
  return fixAllCharData(data);
}

async function getComments(id) {
  const response = await fetch(`${myEndpoint}/comment/${id}`);
  const data = await getDataFromResponse(response);
  return data;
}

async function addComment(comment,id){
 const response = await fetch(`${myEndpoint}/comment/${id}`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(comment)
  });
  const data = await getDataFromResponse(response);
  return data; 
}

async function deleteCommentFromDB (commentId,id) {
  const response = fetch(`${myEndpoint}/comment/${id}`, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({commentId})
  });
}

async function deleteCharacterFromDB (id) {
  const response = fetch(`${myEndpoint}/character/${id}`, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'},
  });
  const data = await getDataFromResponse(response);
  return data; 
}

async function updateCharacter (id) {
  const response = fetch(`${myEndpoint}/character/${id}`, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
  });
  const data = await getDataFromResponse(response);
  return data; 
}

async function getRandomQuote () {
  const response = await fetch(`${endpoint}/quote/random`);
  return getDataFromResponse(response);
}

async function getAllEpisodes () {
  const response = await fetch(`${endpoint}/episodes?series=Breaking+Bad`);
  return getDataFromResponse(response);
}

async function getDataFromResponse (response) {
  if (response) {
    return response.json();
  } else {
    throw new Error("Something wrong happened");
  }
}

async function fixCharData (data) {
  const correctedData = data.filter(element => {return element.category.match(/Breaking Bad/)})
  if (data[0].name === 'Holly White'  ) {
    data[0].img = "https://tvline.com/wp-content/uploads/2013/09/breaking-bad-elanor-anne-wenrich-325.jpg?w=300";
  }
  return correctedData;
}

async function fixAllCharData (data) {
  data.forEach((personage) => {
    if (personage.name === "Holly White") {
      personage.img = "https://tvline.com/wp-content/uploads/2013/09/breaking-bad-elanor-anne-wenrich-325.jpg?w=300";
    }
  })
  return fixCharData(data);
}

export { getDataFromAPI, getActorFromAPIById, filterActorsByName, getRandomQuote, getAllEpisodes,getComments,deleteCommentFromDB,addComment };
