CREATE DATABASE book_loans;

USE book_loans;

/*--tabela dos livros--*/

CREATE TABLE publisher (
    name VARCHAR(25) NOT NULL UNIQUE,
    year INTEGER NOT NULL,
    PRIMARY KEY(name)
);

CREATE TABLE books (
    id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    title VARCHAR(25) NOT NULL UNIQUE,
    year INTEGER NOT NULL,
    author VARCHAR(20) NOT NULL,
    publisher_name VARCHAR(25) NOT NULL UNIQUE,
    PRIMARY KEY (id),
    FOREIGN KEY (publisher_name) REFERENCES publisher (name)
);

/*--tabela dos clientes--*/

CREATE TABLE clients (
    id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    fullname VARCHAR(30) NOT NULL,
    birth_date DATE NOT NULL,
    phone BIGINT NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

/*--tabela das reservas, onde é referenciado o id de cada registo quer das books quer dos clientes--
--cardinallity das reservas -> Many-to-Many---*/

CREATE TABLE reservations (
    id INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    book_id INTEGER NOT NULL UNIQUE,
    client_id INTEGER NOT NULL UNIQUE,
    PRIMARY KEY (id),
    FOREIGN KEY (book_id) REFERENCES books(id),
    FOREIGN KEY (client_id) REFERENCES clients (id)
);

INSERT INTO publisher (name, year)
VALUES
('Paolini LCC', 1992),
('Livros e companhia', 2000),
('Tremoco e companhia', 2002),
('Hedra', 1998),
('Livraria do Primo', 2005),
('Livraria Chardron',1991);

INSERT INTO books (id, title, year, author, publisher_name)
VALUES 
(1,'Eragon', 2002, 'Cristopher Paolini', 'Paolini LCC'),
(2, 'Uma aventura no quarto', 2010, 'Pedro Isidoro', 'Livros e companhia'),
(3, 'Comi um tremoco', 1500, 'Tremoco Figueiras', 'Tremoco e companhia'),
(4, 'Cronicas de Sebastopol', 2017, 'Tolstoi', 'Hedra'),
(5, 'O Primo Basilio', 1878, 'Eca de Queiroz', 'Livraria Chardron'),
(6, 'A Prima do Basilio', 1900, 'Primo do Eca', 'Livraria do Primo');

INSERT INTO clients (id,fullname,birth_date,phone)
VALUES 
(1, 'Joao Faria', 02/02/1992, 961562897),
(2, 'Pedro Miguel', 02/09/1989, 915489635),
(3, 'Diogo Trigueira', 25/12/1986, 934568975),
(4, 'Miguel Diogo', 24/03/1990, 915489658),
(5, 'Joaquim Nicolau', 14/04/2000, 934587628);

INSERT INTO reservations (id, book_id, client_id)
VALUES
(1, 4, 2),
(2, 6, 3),
(3, 5, 4),
(4, 3, 1);


/*--DO WE HAVE ANY BOOKS BY TOLSTOY?--
SELECT * FROM books WHERE author = 'Tolstoi';
--HOW MANY BOOKS ARE OUT ON LOAN?--
SELECT COUNT(*) FROM reservations;
--WHICH COMPANY PUBLISHED "O PRIMO BASILIO"?--
SELECT p.name, b.title FROM books AS b, publisher AS p WHERE b.publisher_name=p.name AND title = 'O Primo Basilio';
--WHICH USERS HAVE BORROWED BOOKS PUBLISHED BEFORE 1974?--
SELECT c.fullname, b.title, b.year FROM clients AS c, books AS b, reservations AS r WHERE b.id=r.book_id AND c.id=r.client_id AND b.year <1974;

*/




