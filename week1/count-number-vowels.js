function countingVowels(phrase) {
  var totalVowels = 0;
  const vowels = ["a", "e", "i", "o", "u"];
  
  for (i = 0; i < phrase.length; i++) {
    if (vowels.includes(phrase[i])) {
      totalVowels++;
    }
  }
  return totalVowels;
}

console.log(countingVowels("hello my name is pedro"));
