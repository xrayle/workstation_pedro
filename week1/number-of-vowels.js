function countingVowels(phrase) {
  const vowels = ["a", "e", "i", "o", "u"];
  var countingVowels = 0;
  
  for (i = 0; i < phrase.length; i++) {
    var char = phrase[i];

    for (j = 0; j < vowels.length; j++) {
      if (char === vowels[j]) {
        countingVowels++;
      }
    }
  }
  return countingVowels;
}
console.log(countingVowels("supercalifragilisticexpialidocious")); 


