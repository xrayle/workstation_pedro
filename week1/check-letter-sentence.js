function checkIfLetterExists(phrase, char) {

    for (var i = 0; i < phrase.length; i++) {
        var currentChar = phrase[i];

        if (currentChar.toLowerCase() === char.toLowerCase()) {
            return true;
        };
    }
    return false;
}

console.log(checkIfLetterExists('Hello World', 'o'));
    
