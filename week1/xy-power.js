function findPowerResult(x, y) {
  var powerResult = x;

  for (i = y; i > 1; i--) {
    powerResult = powerResult * x;
  }
  return powerResult;
}

var finalResult = findPowerResult(9, 3);

console.log(finalResult);
