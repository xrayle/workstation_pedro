function checkPalindrome(word) {
  var backwardIndex = word.length - 1;
  var forwardIndex = 0;
  while (forwardIndex < backwardIndex) {
    if (word[forwardIndex].toLowerCase() != word[backwardIndex].toLowerCase()) {
      return word + " is not a palindrome!";
    }

    forwardIndex++;
    backwardIndex--;
  }

  return word + " is a palindrome";
}

console.log(findPalindrome("batata"));
console.log(findPalindrome("Racecar"));
console.log(findPalindrome("Anna"));
