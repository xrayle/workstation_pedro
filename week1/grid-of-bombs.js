function countingBombs(table) {
  var totalBombs = 0;

  for (var i = 0; i < table.length; i++) {
    var row = table[i];

    for (var j = 0; j < row.length; j++) {
      if (row[j] === 1) {
        totalBombs++;
      }
    }
  }
  return totalBombs;
}
var totalBombsPlanted = countingBombs([
  [0, 0, 0, 0],
  [0, 1, 0, 1],
  [0, 0, 1, 0],
  [0, 0, 0, 0],
]);

console.log(totalBombsPlanted);

