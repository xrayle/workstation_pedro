function findIndexOfChar(tellMeSomething, letter) {
  
  for (i = 0; i < tellMeSomething.length; i++) {
    if (tellMeSomething[i] === letter) {
      return 'Position of the letter ' + tellMeSomething[i] + ' is: ' + i;
    }
  }
}

console.log(findIndexOfChar("hello pedro", "o"));
