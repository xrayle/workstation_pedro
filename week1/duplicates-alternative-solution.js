function findDuplicates(sentence) {
  var duplicates = [];
  var checkedChars = [];

  for (var i = 0; i < sentence.length; i++) {
    var letter = sentence[i];
    
    if (checkedChars.includes(letter) && duplicates.includes(letter)) {
      continue;
    }
    if (
      checkedChars.includes(letter) &&
      duplicates.includes(letter) === false
    ) {
      duplicates.push(letter);
      continue;
    }

    checkedChars.push(letter);
  }

  return duplicates;
}

var mysentence = "And if you dont know, now you know";
var result = findDuplicates(mysentence);
console.log(result);
