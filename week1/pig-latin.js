function findPigLatinWord(normalPhrase) {
  const vowels = ["a", "e", "i", "o", "u"];

  var bunchOfWords = normalPhrase.split(" ");
  var phrasePigLatined = "";

  for (var index = 0; index < bunchOfWords.length; index++) {
    var wordAlone = bunchOfWords[index];
    for (var i = 0; i < wordAlone.length; i++) {
      if (i === 0) {
        var eachWord = wordAlone;
        if (vowels.includes(wordAlone[i])) {
          eachWord = bunchOfWords[index] + "way";
          break;
        } else {
          var removedLetter = wordAlone[i];
          continue;
        }
      }
      if (vowels.includes(wordAlone[i])) {
        eachWord = eachWord.slice(i);
        eachWord = eachWord + removedLetter;
        eachWord = eachWord + "ay";
        break;
      } else {
        eachWord = eachWord + removedLetter + wordAlone[i];
      }
    }
    phrasePigLatined = phrasePigLatined + eachWord + " ";
  }
  return phrasePigLatined;
}

console.log(findPigLatinWord("im an happy child"));
