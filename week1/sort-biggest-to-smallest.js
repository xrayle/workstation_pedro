var numbers = [20, 10, 8, 80, -1, 2, -20, 3];

function sortBiggestToSmallest(numbers) {
  var sortedList = [];
  var size = numbers.length;

  while (sortedList.length < size) {
    var position = null;
    var biggest;

    for (var i = 0; i < numbers.length; i++) {
      var currentNumber = numbers[i];

      if (i === 0) {
        biggest = currentNumber;
        position = i;
      }

      if (biggest < currentNumber) {
        biggest = currentNumber;
        position = i;
      }
    }

    sortedList.push(biggest);
    numbers.splice(position, 1);
  }
  return sortedList;
}

console.log(sortBiggestToSmallest(numbers));
