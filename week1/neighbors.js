var numbersGiven = [-2, 0, 4, 26, 112, 113, 220];
var numbersGiven2 = [-2, 0, 4, 26, 112, 0, 1];
var numbersGiven3 = [1, 0, 4, 26, 112, 113, 220];
var index;

function findNeighbors(numbersGiven) {
  var neighbors = [];
  for (var i = 0; i < numbersGiven.length - 1; i++) {
    var difference = Math.abs(numbersGiven[i] - numbersGiven[i + 1]);

    if (i === 0) {
      var smallestDifference = difference;
      index = i;
    }

    if (difference < smallestDifference) {
      smallestDifference = difference;
      index = i;
    }
  }
  neighbors.push(numbersGiven[index]);
  neighbors.push(numbersGiven[index + 1]);
  return neighbors;
}
var small = findNeighbors(numbersGiven);
var small1 = findNeighbors(numbersGiven2);
var small2 = findNeighbors(numbersGiven3);

console.log(small);
console.log(small1);
console.log(small2);
