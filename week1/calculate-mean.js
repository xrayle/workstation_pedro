function calculateMean(myNumbers) {
  var total = 0;
  var mean = 0;

  for (var i = 0; i < myNumbers.length; i++) {
    total = total + myNumbers[i];
  }
  mean = total / myNumbers.length;
  return mean;
}

console.log(calculateMean([25, 56, 45, 20]));
