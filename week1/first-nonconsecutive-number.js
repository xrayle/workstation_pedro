function findConsecutiveNumbers(giveMeTheNumbers) {
  var nonConsecutiveNumbers = [];

  for (i = 0; i < giveMeTheNumbers.length - 1; i++) {
    if (giveMeTheNumbers[i] + 1 !== giveMeTheNumbers[i + 1]) {
      nonConsecutiveNumbers.push(giveMeTheNumbers[i]);
      nonConsecutiveNumbers.push(giveMeTheNumbers[i + 1]);
    }
  }
  return nonConsecutiveNumbers;
}

console.log(findConsecutiveNumbers([1, 2, 3, 4, 6]));
