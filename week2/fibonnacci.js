function calculateFibonacciValue(position) {
  var fibonacciValue;
  var numberBefore;
  var numberAfter;

  for (var i = 0; i <= position - 1; i++) {

    if (i <= 1) {
      fibonacciValue = 1;
      numberAfter = i;
      numberBefore = 0;

    } else {
      numberBefore = numberAfter;
      numberAfter = fibonacciValue;
      fibonacciValue = numberAfter + numberBefore;
    }
  }
  return fibonacciValue;
}
console.log(calculateFibonacciValue(21));
console.log(calculateFibonacciValue(20));