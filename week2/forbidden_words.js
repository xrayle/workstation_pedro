function filterSentenceForbiddenWords(phrase, forbiddenWords) {

  var filteredPhrase = phrase;
  for (var i = 0; i < forbiddenWords.length; i++) {

    var forbiddenWord = forbiddenWords[i];

    var startingIndex;
    while ((startingIndex = filteredPhrase.indexOf(forbiddenWord, startingIndex) !== -1)) {
      filteredPhrase = filteredPhrase.replace(forbiddenWord, '')
    }
  }

  return filteredPhrase.trim();
}
console.log(
  filterSentenceForbiddenWords(
    "this is a dish with salmon, potatoes and salad",
    ["potatoes", "salad"]
  )
);
console.log(
  filterSentenceForbiddenWords(
    "i love to eat lasanha, pancaques, pizza and much more",
    ["love", "pizza"]
  )
);
